elkcode (10.2.4-2) UNRELEASED; urgency=medium


 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 12 Jan 2025 21:04:47 +0100

elkcode (10.2.4-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/makeflags.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/ls_locale_for_reproducible_order.patch: Likewise.
  * debian/patches/makeflags.patch: Use system xc_f03_lib_m.mod and add
    /usr/include as include dir in order to find it.

 -- Michael Banck <mbanck@debian.org>  Sun, 12 Jan 2025 21:04:09 +0100

elkcode (9.6.8-1) unstable; urgency=medium

  * New usptream release.
  * debian/patches/makeflags.patch: Refreshed.

 -- Michael Banck <mbanck@debian.org>  Sun, 30 Jun 2024 22:39:51 +0200

elkcode (9.2.12-1) unstable; urgency=medium

  * New usptream release.
  * debian/patches/ls_locale_for_reproducible_order.patch: Refreshed.

 -- Michael Banck <mbanck@debian.org>  Fri, 29 Dec 2023 16:36:33 +0100

elkcode (9.2.5-1) unstable; urgency=medium

  * New usptream release.
  * debian/patches/makeflags.patch: Refreshed. 

 -- Michael Banck <mbanck@debian.org>  Sat, 02 Dec 2023 18:36:52 +0100

elkcode (9.1.15-1) unstable; urgency=medium

  * New usptream release.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Use secure URI in debian/watch.
  * Bump debhelper from old 12 to 13.

  [ Michael Banck ]
  * debian/patches/default_data_directory.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/ls_locale_for_reproducible_order.patch: Likewise.
  * debian/rules (override_dh_auto_build): No longer assemble make.inc from
    make.def and make.inc.in.
  * debian/patches/makeflags.patch: Update, and add -lfftw3f to LIBS.
  * debian/rules (override_dh_auto_clean): Removed, no longer needed.

 -- Michael Banck <mbanck@debian.org>  Sun, 15 Oct 2023 22:04:16 +0200

elkcode (8.4.30-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/makeflags.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/ls_locale_for_reproducible_order.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Mon, 04 Jul 2022 21:06:06 +0200

elkcode (8.3.22-2) unstable; urgency=medium

  * debian/patches/makeflags.patch: Likewise.
  * debian/patches/makefile_clean.patch: Refreshed.
  * debian/patches/ls_locale_for_reproducible_order.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Sat, 12 Mar 2022 18:47:48 +0100

elkcode (8.3.22-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Banck <mbanck@debian.org>  Sat, 12 Mar 2022 17:56:44 +0100

elkcode (7.2.42-2) unstable; urgency=medium

  * Upload to unstable (Closes: #1003005).
  * debian/control (Build-Depends): Replaced debhelper with debhelper-compat (=
    12).
  * debian/compat: Removed.
  * debian/rules (override_dh_auto_build): Disable parallelism during build.
  * debian/control (Build-Depends): Added libwannier90-dev.
  * debian/patches/makeflags.patch: Link to libwannier90.

 -- Michael Banck <mbanck@debian.org>  Sun, 02 Jan 2022 20:43:14 +0100

elkcode (7.2.42-1) experimental; urgency=medium

  * New upstream release.
  * debian/patches/ls_locale_for_reproducible_order.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/makeflags.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Sun, 10 Oct 2021 14:10:30 +0200

elkcode (6.8.4-1) experimental; urgency=medium

  * New upstream release.
  * debian/patches/ls_locale_for_reproducible_order.patch: New patch, makes the
    build reproducable, by Alexis Bienvenüe (Closes: #824653).
  * debian/patches/perl_5_30_protex.patch: Removed, applied upstream.
  * debian/patches/default_data_directory.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/makeflags.patch: Likewise.
  * debian/patches/makeflags.patch: Update SRC_libxc variable.
  * debian/control (Build-Depends): Add versioned build dependency on
    libxc-dev >= 5.0.0-1.

 -- Michael Banck <mbanck@debian.org>  Sun, 03 Jan 2021 11:21:39 +0100

elkcode (6.3.2-2) unstable; urgency=medium

  * debian/patches/makeflags.patch: Add -fallow-argument-mismatch to F90_OPTS
    and F77_OPTS (Closes: #957168).

 -- Michael Banck <mbanck@debian.org>  Sat, 03 Oct 2020 12:16:50 +0200

elkcode (6.3.2-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/perl_5_30_protex.patch: New patch, fixes the protex script
    for Perl 5.30, taken from the upstream development forum, by John Kay
    Dewhurst (Closes: #952357).
  * debian/control (Build-Depends): Replaced texlive-latex-base with
    texlive-latex-extra, as required style files were moved to it.
  * debian/tests/testsuite.sh: Use bash to run tests_quick.sh script.
  * debian/patches/testsuite_quick.patch: Updated for renamed test directories
    and proper exit status added.

 -- Michael Banck <mbanck@debian.org>  Sun, 23 Feb 2020 16:07:03 +0100

elkcode (6.2.8-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/default_data_directory.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/makeflags.patch: Likewise.
  * debian/patches/makeflags.patch: Generate make.inc.in, not make.inc.
  * debian/rules (override_dh_auto_build): Assemble make.inc from make.def and
    make.inc.in before build.
  * debian/rules (override_dh_auto_clean): New target, remove generated
    make.inc.

 -- Michael Banck <mbanck@debian.org>  Thu, 01 Aug 2019 20:14:50 +0200

elkcode (5.4.24-2) unstable; urgency=medium

  * debian/patches/testsuite_quick.patch: Use bash as interpreter, print
    testsuite output to stdout in addition to logfile and add 'set -o
    pipefail'.
  * debian/rules (override_dh_auto_test): Use bash to run testsuite.

 -- Michael Banck <mbanck@debian.org>  Thu, 10 Jan 2019 15:19:40 +0100

elkcode (5.4.24-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/makefile_clean.patch: Refreshed.
  * debian/patches/makeflags.patch: Likewise.
  * debian/control (elk-lapw/Depends): Added mpi-default-bin (Closes: #918053).
  * debian/patches/makeflags.patch: Added MKL/OpenBLAS/BLIS stub files.
  * debian/tests: Add autopkgtests.
  * debian/patches/testsuite_quick.patch: Allow for external elk executable.

 -- Michael Banck <mbanck@debian.org>  Wed, 09 Jan 2019 19:34:48 +0100

elkcode (5.2.14-1) unstable; urgency=medium

  * New upstream release.

  [ Michael Banck ]
  * debian/patches/default_data_directory.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/makeflags.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Mon, 31 Dec 2018 16:30:09 +0100

elkcode (4.0.15-2) unstable; urgency=medium

  * debian/patches/makeflags.patch: Link against libxcf90 as well.

 -- Michael Banck <mbanck@debian.org>  Sun, 16 Oct 2016 19:38:24 +0200

elkcode (4.0.15-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/default_data_directory.patch: Refreshed.
  * debian/patches/makefile_clean.patch: Likewise.
  * debian/patches/makeflags.patch: Likewise.
  * debian/control (Standards-Version): Bumped to 3.9.6.
  * debian/control (Description): Updated.
  * debian/control (Elk/Suggests): Added xcrysden.
  * debian/patches/makefile_clean.patch: Updated to properly remove main
    executable on clean.

 -- Michael Banck <mbanck@debian.org>  Sun, 16 Oct 2016 10:38:05 +0200

elkcode (2.3.22-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/makeflags.patch: Split out clean-target specific hunks in
    Makefiles into ...
  * debian/patches/makefile_clean.patch: ... this new patch.
  * debian/rules (override_dh_auto_build): New target, build the TeX-based
    PDF-manuals as well.
  * debian/control (Build-Depends): Added texlive-latex-base.
  * debian/patches/makefile_clean.patch: Clean elk.out and elk.lst (generated
    during PDF build) as well.
  * debian/elk-lapw.docs: Install built PDF files from under src/, not the the
    shipped ones from docs/.
  * debian/patches/makeflags.patch: Updated and refreshed.
  * debian/patches/testsuite_quick.patch: Added test case #18.
  * debian/patches/elk.fftw3.zfftifc.f90.patch: Removed, no longer needed.
  * debian/patches/makefile_clean.patch: Refreshed.

 -- Michael Banck <mbanck@debian.org>  Sun, 26 Oct 2014 23:36:38 +0100

elkcode (2.2.10-2) unstable; urgency=low

  [ Daniel Leidert ]
  * debian/control (Build-Depends): Added libfftw3-dev.
  * debian/copyright: Minor update.
  * debian/patches/elk.fftw3.zfftifc.f90.patch: Added (taken from Fedora).
    - Compile with FFTW3.
  * debian/patches/makeflags.patch: Adjusted.
    - Build with FFTW3 and exclude all internal copies from compilation.
  * debian/patches/series: Adjusted.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Fri, 21 Feb 2014 00:41:22 +0100

elkcode (2.2.10-1) unstable; urgency=low

  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.5.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 15 Dec 2013 20:40:07 +0100

elkcode (2.2.5-1) unstable; urgency=low

  * New upstream release.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 20 Oct 2013 22:23:47 +0200

elkcode (2.2.1-1) unstable; urgency=low

  * New upstream release.

  [ Daniel Leidert ]
  * debian/control (Build-Depends): Added libxc-dev, mpi-default-dev and
    pkg-config.
  * debian/copyright: Added copyright note about elk-bands.
  * debian/elk-bands.1: Added.
  * debian/elk-lapw.1: Ditto.
  * debian/elk-lapw.manpages: Ditto.
  * debian/patches/makefile_clean.patch: Merged into makeflags.patch.
  * debian/patches/skip_blas_lapack.patch: Ditto.
  * debian/patches/makeflags.patch: Extended.
    - Merged contents of debian/patches/makefile_clean.patch.
    - Merged contents of debian/patches/skip_blas_lapack.patch.
    - Enabled MPI support (use mpif90 and mpif77 instead of gfortran).
    - Enabled support for libxc.
  * debian/patches/series: Adjusted.

  [ Michael Banck ]
  * debian/control (Build-Depends): Added mpi-default-bin.
  * debian/elk-lpaw.examples: New file, install the upstream examples directory
    instead of the testsuite input files.
  * debian/rules (override_dh_installexamples): Removed.
  * debian/rules (override_dh_auto_test): Set OMPI_MCA_orte_rsh_agent
    environment variable to make test suite run in a chroot without
    warnings/errors from OpenMPI.

 -- Michael Banck <mbanck@debian.org>  Mon, 23 Sep 2013 17:25:45 +0200

elkcode (2.1.25-2) unstable; urgency=low

  [ Daniel Leidert ]
  * debian/rules: Disable parallel builds to fix FTBFS (closes: #720837).
    (override_dh_install): Rename binary to elk-lapw (closes: #720044).
  * debian/patches/makeflags.patch: Adjusted.
    - Use dpkg-buildflags to get the flags and enable hardening.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Tue, 27 Aug 2013 22:50:04 +0200

elkcode (2.1.25-1) unstable; urgency=low

  [ Daniel Leidert ]
  * New upstream release.
  * debian/control (Standards-Version): Bumped to 3.9.4.
    (Vcs-Browser, Vcs-Svn): Fixed vcs-field-not-canonical.
    (DM-Upload-Allowed): Dropped.
    (Standards-Version): Bumped to 3.9.4.
  * debian/copyright: Fixed format.
  * debian/elk-lapw.doc-base: Added.
  * debian/elk-lapw.docs: Removed release notes.
  * debian/elk-lapw.install: Added.
  * debian/rules: Enabled parallel build. Rename Perl script. Install release
    notes as upstream changelog.
    (override_dh_auto_install): Dropped and used elk-lapw.install instead.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sat, 17 Aug 2013 21:32:52 +0200

elkcode (1.4.22-1) unstable; urgency=low

  * Initial release (Closes: #706538).

 -- Michael Banck <mbanck@debian.org>  Thu, 11 Jul 2013 22:18:34 +0200
